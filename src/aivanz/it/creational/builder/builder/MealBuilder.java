package aivanz.it.creational.builder.builder;

import aivanz.it.creational.builder.product.Meal;

public abstract class MealBuilder {
	public abstract void AddSandwich();

	public abstract void AddSideOrder();

	public abstract void AddDrink();

	public abstract void AddOfferItem();

	public abstract void SetPrice();

	public abstract Meal GetMeal();
}
