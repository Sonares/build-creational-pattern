package aivanz.it.creational.builder.product;

public class Meal {

	private String Sandwich;
	private String SideOrder;
	private String Drink;
	private String Offer;
	private double Price;

	public Meal() {

	}

	public String getSandwich() {
		return Sandwich;
	}

	public void setSandwich(String sandwich) {
		Sandwich = sandwich;
	}

	public String getSideOrder() {
		return SideOrder;
	}

	public void setSideOrder(String sideOrder) {
		SideOrder = sideOrder;
	}

	public String getDrink() {
		return Drink;
	}

	public void setDrink(String drink) {
		Drink = drink;
	}

	public String getOffer() {
		return Offer;
	}

	public void setOffer(String offer) {
		Offer = offer;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	@Override
	public String toString() {
		return "Sandwich = " + Sandwich + "\nSideOrder = " + SideOrder + "\nDrink = " + Drink + "\nOffer = " + Offer
				+ "\nPrice = " + Price;
	}

}