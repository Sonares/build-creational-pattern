package aivanz.it.creational.builder.builder;

import aivanz.it.creational.builder.product.Meal;

public class MischievousMexicanBuilder extends MealBuilder {
	private Meal _meal = new Meal();

	@Override
	public void AddSandwich() {
		_meal.setSandwich("Spicy burger");
	}

	@Override
	public void AddSideOrder() {
		_meal.setSideOrder("Nachos");
	}

	@Override
	public void AddDrink() {
		_meal.setDrink("Tequila");
	}

	@Override
	public void AddOfferItem() {
		_meal.setOffer("Hat");
	}

	@Override
	public void SetPrice() {
		_meal.setPrice(5.49);
	}

	@Override
	public Meal GetMeal() {
		return _meal;
	}

}