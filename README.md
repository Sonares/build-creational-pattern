#Builder
This is a simple example of Builder, creational design pattern.  

**Description:**  
The builder pattern is used to create complex objects with constituent parts that must be created in the same order or using a specific algorithm. An external class controls the construction algorithm.  

![PlantUML model] (http://www.plantuml.com/plantuml/png/VP4n2uCm48Nt-nKtMef2EqefqXQwEdQDoGs2DN0vkrZ_UnCqKYIuXVVoxdsvL4uKo_3kG7VAENoqJ5eiumZ7k-sTyA0bQuQsCyGbZghnaj8otMRfKEB7jWOcW6ZXF6cv40PjKhLlF6KncNBFk-uhvJv9QhR6qmPtEGQicSQ_HJC9_L6bM584gTxzjYZW6AHrfJRFurPWorCKrpLLOXDKYDxbwuER05JK6_zjFm00)  
