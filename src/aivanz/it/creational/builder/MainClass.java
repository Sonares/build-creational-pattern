package aivanz.it.creational.builder;

import aivanz.it.creational.builder.builder.JollyVegetarianMealBuilder;
import aivanz.it.creational.builder.builder.MealBuilder;
import aivanz.it.creational.builder.builder.MischievousMexicanBuilder;
import aivanz.it.creational.builder.product.Meal;

/* Creational pattern, it controls class instantiation. 
 * The builder pattern is used to create complex objects with constituent parts that must be created 
 * in the same order or using a specific algorithm. An external class, known as the director, controls 
 * the construction algorithm. */

/* Example:
 * Fast food restaurant system. 
 * It may be that each time a standard meal is prepared, it can include one burger or sandwich, one 
 * side order, a drink and a special offer item. The employee taking the order selects the type of meal 
 * and the builder pattern is used to determine exactly what goes into each meal. */

/* Director:
 * +Construct(builder: Builder): void
 * In this case: MealDirector.
 * Main class. Controls the algorithm that creates the final object. In its method MakeMeal(), there are 
 * all the method to create the meal. To decide what meal you want, pass the appropriate 
 * MealBuilder (Builder abstract class).
 * 
 * Builder:
 * +BuildPart1(): void
 * +BuildPart2(): void
 * +BuildPartX(): void
 * +GetProduct(): Product
 * In this case: MealBuilder.
 * An abstract class that specifies all the single object's parts to build, and a final method to get the "assembled" object.
 * Each method, generally, is abstract as the actual functionality of the builder is carried out in the concrete subclasses.
 * 
 * ConcreteBuilder extends Builder:
 * +BuildPart1(): void
 * +BuildPart2(): void
 * +BuildPartX(): void
 * +GetProduct(): Product
 * In this case: JollyVegetarianMealBuilder and MischievousMexicanBuilder.
 * This is the real builder class. This class sets the object parameters individually. Has a method to retrieve the final 
 * object.
 * 
 * Product:
 * In this case: Meal.
 * The product class that has to be built. 
 * 
 * */

public class MainClass {
	public static void main(String args[]) {

		MealDirector director = new MealDirector();

		MealBuilder jvmb = new JollyVegetarianMealBuilder();
		director.MakeMeal(jvmb);
		Meal vegMeal = jvmb.GetMeal();
		System.out.println(vegMeal);

		System.out.println();

		MealBuilder mmmb = new MischievousMexicanBuilder();
		director.MakeMeal(mmmb);
		Meal mexMeal = mmmb.GetMeal();
		System.out.println(mexMeal);

	}
}
