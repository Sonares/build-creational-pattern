package aivanz.it.creational.builder;

import aivanz.it.creational.builder.builder.MealBuilder;

public class MealDirector {
	public void MakeMeal(MealBuilder mealBuilder) {
		mealBuilder.AddSandwich();
		mealBuilder.AddSideOrder();
		mealBuilder.AddDrink();
		mealBuilder.AddOfferItem();
		mealBuilder.SetPrice();
	}
}