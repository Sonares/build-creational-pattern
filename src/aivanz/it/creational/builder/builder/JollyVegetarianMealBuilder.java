package aivanz.it.creational.builder.builder;

import aivanz.it.creational.builder.product.Meal;

public class JollyVegetarianMealBuilder extends MealBuilder {
	private Meal _meal = new Meal();

	@Override
	public void AddSandwich() {
		_meal.setSandwich("Vegeburger");
	}

	@Override
	public void AddSideOrder() {
		_meal.setSideOrder("Fries");
	}

	@Override
	public void AddDrink() {
		_meal.setDrink("Orange juice");
	}

	@Override
	public void AddOfferItem() {
		_meal.setOffer("Donut voucher");
	}

	@Override
	public void SetPrice() {
		_meal.setPrice(4.99);
	}

	@Override
	public Meal GetMeal() {
		return _meal;
	}

}